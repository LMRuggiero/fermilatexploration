.. FermiLatExploration documentation master file, created by
   sphinx-quickstart on Tue Feb 15 11:15:58 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FermiLatExploration's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
